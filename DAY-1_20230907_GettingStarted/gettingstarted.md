#### Session Video:
    https://drive.google.com/file/d/13_gIDgrAlBKVCnAmNik2ovfhIHs8PVHi/view?usp=sharing

#### Getting Started

Devices :
    
    - Laptops (Host Machine):
        - DELL: Windows : 2010 , 2011
        - APPLE : MacOS 
        - DELL: Linux (Debian/Ubuntu/Redhat/CentOS/AmazonLinux/Oracle)

    - Desktops :
        - Windows
        - MacOS 
        - Linux (Debian/Ubuntu/Redhat/CentOS/AmazonLinux/Oracle)
